import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { User } from 'src/app/modules/account/models/user.model';

@Component({
  selector: 'app-menue',
  templateUrl: './menue.component.html',
})
export class MenueComponent implements OnInit {
  @Input() user: User;
  @Output() toggleMenu: EventEmitter<boolean> = new EventEmitter();
  constructor() {}

  get isUser(): boolean {
    return !!(this.user);
  }

  ngOnInit(): void {

  }

  onToggleMenu(): void {
    this.toggleMenu.emit(true);
  }
}
