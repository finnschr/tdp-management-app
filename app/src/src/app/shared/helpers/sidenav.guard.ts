import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import * as fromStore from '../src/app/shared/store';

import { Store } from '@ngrx/store';

@Injectable({ providedIn: 'root' })
export class SidenavGuard implements CanActivate {
    constructor(private store: Store<fromStore.AppState>) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (route.data.hasOwnProperty('sidenav') && route.data.sidenav) {
            this.store.dispatch(new fromStore.OpenSidenavAction());
            return true;
        } else {
            this.store.dispatch(new fromStore.CloseSidenavAction());
            return true;
        }
    }
}
