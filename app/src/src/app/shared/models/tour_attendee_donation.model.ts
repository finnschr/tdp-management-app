import { MeanOfTransportationModel } from './mean_of_transportaion.model';
import { Point } from './point.model';

export enum Unit {
    KM = 'km',
    M = '',
}

export interface TourAttendeeDonation {
    id: number;
    point: Point;
    date: string;
    amount: number;
    by: string|MeanOfTransportationModel;
    unit: Unit.KM | Unit.M;
    createdAt?: string;
}
