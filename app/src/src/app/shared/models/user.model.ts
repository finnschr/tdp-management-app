import { Attendee } from '../../modules/account/models/attendee_model';

export class User {
    userId: string;
    username: string;
    mail?: string;
    attendee: Attendee;
}

export class UserToken {
    accessToken: string;
    refreshToken: string;
    userValues: User;
}
