import { FormControl, Validators } from '@angular/forms';

export interface TourPoint {
    latitude: number;
    longitude: number;
    name?: string;
    date: string;
    id?: number;
}

export class TourPointForm {
    id = new FormControl();
    latitude = new FormControl();
    longitude = new FormControl();
    name = new FormControl();
    pointDate = new FormControl();

    constructor(point: TourPoint) {
        if (point.id) {
            this.id.setValue(point.id);
        }
        this.latitude.setValue(point.latitude);
        this.latitude.setValidators(Validators.required);
        this.longitude.setValue(point.longitude);
        this.longitude.setValidators(Validators.required);
        this.name.setValue(point.name);
        this.name.setValidators(Validators.required);
        this.pointDate.setValue(point.date);
        this.pointDate.setValidators(Validators.required);
    }
}
