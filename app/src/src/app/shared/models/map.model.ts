import { TourEvent } from './tour_event.model';
import { TourModel } from './tour.model';
import { TourAttendeeDonation } from 'src/app/shared/models/tour_attendee_donation.model';

export interface MapModel {
    events: TourEvent[];
    tours: TourModel[];
    donations: TourAttendeeDonation[];
}
