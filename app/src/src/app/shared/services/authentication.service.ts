import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { map, catchError } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import { RegistrationModel } from '../../modules/auth/models/registration.model';
import { UserToken } from '../models/user.model';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserTokenSubject: BehaviorSubject<UserToken>;
    public currentUserToken: Observable<UserToken>;

    constructor(private http: HttpClient) {
        this.currentUserTokenSubject = new BehaviorSubject<UserToken>(JSON.parse(localStorage.getItem('currentUserToken')));
        this.currentUserToken = this.currentUserTokenSubject.asObservable();
    }
    create(registration: RegistrationModel): Observable<{} | string> {
      return this.http
            .post<RegistrationModel>(`${environment.backendUrl}/register`, {
                username: registration.username,
                email: registration.mail,
                plainPassword: registration.password,
                newsletterAccepted: false,
                termsAccepted: true
            })
            .pipe(catchError((error: any) => {
                return Observable.throw(error);
            }));
    }
    public get currentUserTokenValue(): UserToken {
        return this.currentUserTokenSubject.value;
    }

    public get isUser(): boolean {
      return this.currentUserTokenValue !== null;
    }

    login(username: string, plainPassword: string): Observable<UserToken> {
        return this
          .http
          .post<UserToken>(environment.backendUrl + '/login_check', { username, plainPassword })
            .pipe(
                map((token: UserToken) => {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('currentUserToken', JSON.stringify(token));
                this.currentUserTokenSubject.next(token);
                return token;
            }),
            catchError((error: any) => {
                this.currentUserTokenSubject.next(null);
                return Observable.throw(error);
            })
            );
    }

    logout(): Observable<UserToken> {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUserToken');
        this.currentUserTokenSubject.next(null);

        return this.currentUserToken;
    }
}
