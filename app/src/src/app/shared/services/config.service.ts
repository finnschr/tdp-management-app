import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  private config: {[id: string]: string};

  constructor() { }

  setConfig(config: {[id: string]: string}): void {
    this.config = config;
  }

  getValue = (key: string): string => {
    for (const listKey in this.config) {
      if (listKey === key) {
        return this.config[listKey];
      }
    }

    return null;
  }
}
