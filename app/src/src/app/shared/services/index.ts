import { DataService } from './data.service';
import { LocationApiService } from './location_api.service';
import { ConfigService } from './config.service';


export const services: any[] = [
    DataService,
    LocationApiService,
    ConfigService
];

export * from './data.service';
export * from './location_api.service';
export * from './config.service';
