import { ActionReducerMap, createFeatureSelector } from '@ngrx/store';
import * as fromTour from './tour.reducer';
import * as fromEvents from './tour_events.reducer';
import * as fromAttendee from './attendee.reducer';
import * as fromMap from './map_data.reducer';
import * as fromMOT from './mot_data.reducer';

export interface AppState {
    tours: fromTour.ToursState;
    events: fromEvents.EventsState;
    donations: fromAttendee.DonationState;
    map: fromMap.MapDataState;
    mot: fromMOT.MeansOfTransportationState;
}

export const reducers: ActionReducerMap<AppState> = {
    tours: fromTour.reducer,
    events: fromEvents.reducer,
    donations: fromAttendee.reducer,
    map: fromMap.reducer,
    mot: fromMOT.reducer
};

export const getAppState = createFeatureSelector<AppState>('app');
