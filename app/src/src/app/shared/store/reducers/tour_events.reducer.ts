import * as fromEvents from '../actions/events.action';
import { TourEvent } from 'src/app/shared/models/tour_event.model';

export interface EventsState {
    entities: {[id: number]: TourEvent };
    loaded: boolean;
    loading: boolean;
}

export const initialState: EventsState = {
    entities: {},
    loaded: false,
    loading: false,
};

export function reducer(
    state = initialState,
    action: fromEvents.EventsAction
): EventsState {
    switch (action.type) {
        case fromEvents.LOAD_EVENTS:
        case fromEvents.CREATE_NEW_EVENT:
        case fromEvents.UPDATE_EVENT:
        case fromEvents.REMOVE_EVENT: {
            return {
                ...state,
                loading: true,
            };
        }

        case fromEvents.LOAD_EVENTS_FAIL:
        case fromEvents.REMOVE_EVENT_FAIL:
        case fromEvents.UPDATE_EVENT_FAIL:
        case fromEvents.CREATE_NEW_EVENT_FAIL: {
            return {
                ...state,
                loading: false,
                loaded: false,
            };
        }

        case fromEvents.LOAD_EVENTS_SUCCESS: {
            const events = action.payload;

            const entities = events.reduce(
                (e: { [id: number]: TourEvent }, event: TourEvent) => {
                    return { ...e, [event.id]: event};
                },
                { ...state.entities}
            );

            return {
                ...state,
                loading: false,
                loaded: true,
                entities,
            };
        }

        case fromEvents.CREATE_NEW_EVENT_SUCCESS: {
            const event = action.payload;

            const entities = {...state.entities, [event.id]: event};

            return {
                ...state,
                loading: false,
                loaded: true,
                entities
            };
        }

        case fromEvents.UPDATE_EVENT_SUCCESS: {
            const event = action.payload;

            const entities = {...state.entities, [event.id]: event};

            return {
                ...state,
                loading: false,
                loaded: true,
                entities
            };
        }

        case fromEvents.REMOVE_EVENT_SUCCESS: {
            const id = action.payload;
            const entities = { ... state.entities};
            delete entities[id];

            return {
                ...state,
                loading: false,
                loaded: true,
                entities
            };
        }
    }

    return state;
}

export const getEventEntities = (state: EventsState) => state.entities;
export const getLoading = (state: EventsState) => state.loading;
export const getLoaded = (state: EventsState) => state.loaded;
