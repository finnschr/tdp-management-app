import {createSelector} from '@ngrx/store';

import * as fromFeature from '../reducers';
import * as fromTourEvents from '../reducers/tour_events.reducer';

export const getTourEventsState = createSelector(
    fromFeature.getAppState,
    (state: fromFeature.AppState) => state.events
);

export const getTourEventEntities = createSelector(
    getTourEventsState,
    fromTourEvents.getEventEntities
);


export const getAllTourEvents = createSelector(getTourEventEntities, entities => {
    return Object.keys(entities).map(id => entities[parseInt(id, 10)]);
});

export const getTourEventsLoaded = createSelector(
    getTourEventsState,
    fromTourEvents.getLoaded
);
export const getTourEventsLoading = createSelector(
    getTourEventsState,
    fromTourEvents.getLoading
);
