import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AttendeeDonationComponent } from './attendee-donation/attendee-donation.component';
import { AddAttendeeDonationComponent } from './add-attendee-donation/add-attendee-donation.component';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { DonationRowComponent } from './donation-row/donation-row.component';

const DONATION_ROUTES: Routes = [
  { path: 'list', component: AttendeeDonationComponent},
  { path: 'add-donation', component: AddAttendeeDonationComponent },
];

@NgModule({
  declarations: [
    AttendeeDonationComponent,
    AddAttendeeDonationComponent,
    DonationRowComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(DONATION_ROUTES),
  ],
  providers: [],
  exports: [
    AttendeeDonationComponent,
  ]
})
export class DonationsModule { }
