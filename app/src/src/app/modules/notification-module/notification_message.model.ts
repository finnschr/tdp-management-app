export interface NotificationMessageInterface {
  text: string;
  title: string;
  keepAfterNavigationChange: boolean;
}

export class NotificationMessageInterface implements NotificationMessageInterface {
  constructor(public text: string, public title: string = null, public keepAfterNavigationChange: boolean = false) {}
}

export enum NotificationType {
  WARN = 'warning',
  ERROR = 'error',
  INFO = 'info',
  SUCCESS = 'success'
}

export type NotificationTypes = NotificationType.WARN | NotificationType.ERROR | NotificationType.INFO | NotificationType.SUCCESS;
