import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { NavigationStart, Router } from '@angular/router';
import { NotificationType, NotificationTypes } from './notification_message.model';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  private subject = new Subject<{type: NotificationTypes, title: string, text: string}>();
  private keepAfterNavigationChange = false;

  constructor(private router: Router) {
      // clear alert message on route change
      this.router.events.subscribe(event => {
          if (event instanceof NavigationStart) {
              if (this.keepAfterNavigationChange) {
                  // only keep for a single location change
                  this.keepAfterNavigationChange = false;
              } else {
                  // clear alert
                  this.subject.next();
              }
          }
      });
  }

  success(title: string, text: string = null,  keepAfterNavigationChange: boolean = false) {
      this.keepAfterNavigationChange = keepAfterNavigationChange;
      this.subject.next({ type: NotificationType.SUCCESS, title, text });
  }

  error(title: string, text: string = null,  keepAfterNavigationChange: boolean = false) {
      this.keepAfterNavigationChange = keepAfterNavigationChange;
      this.subject.next({ type: NotificationType.ERROR, title, text });
  }

  warn(title: string, text: string = null,  keepAfterNavigationChange: boolean = false) {
    this.keepAfterNavigationChange = keepAfterNavigationChange;
    this.subject.next({ type: NotificationType.WARN, title, text});
  }

  info(title: string, text: string = null,  keepAfterNavigationChange: boolean = false) {
    this.keepAfterNavigationChange = keepAfterNavigationChange;
    this.subject.next({ type: NotificationType.INFO, title, text });
  }

  getMessage(): Observable<any> {
      return this.subject.asObservable();
  }

  close(): void {
      this.subject.next(null);
  }
}
