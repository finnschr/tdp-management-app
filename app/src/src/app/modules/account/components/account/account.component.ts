import { Component, OnInit, Input } from '@angular/core';
import { User } from '../../models/user.model';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html'
})
export class AccountComponent implements OnInit {
  user: User;
  @Input() title: string;

  constructor(private authService: AuthenticationService) {}

  get isUser(): boolean {
    return !!(this.user);
  }

  ngOnInit(): void {
    this.authService.currentUserToken.subscribe(ut => {
      this.user = ut.userValues;
    });
  }
}
