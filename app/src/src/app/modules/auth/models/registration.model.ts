export interface RegistrationModel {
    username: string;
    password: string;
    passwordConfirm: string;
    mail: string;
    newsletterAccepted: boolean;
    termsAccepted: boolean;
}
