import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';
import { NotificationService } from 'src/app/modules/notification-module/notification.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  returnUrl: string;
  constructor(
    private fb: FormBuilder,
    private authenticationservice: AuthenticationService,
    private router: Router,
    private notify: NotificationService,
    private activeRoute: ActivatedRoute
    ) {
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  ngOnInit(): void {
    this.activeRoute.paramMap.subscribe((params) => {
      this.returnUrl = params.get('returnUrl');
    });
  }

  get invalid(): boolean {
    return this.passwordControlInvalid || this.usernameControlInvalid;
  }

  get passwordControl(): FormControl {
    return this.loginForm.get('password') as FormControl;
  }

  get passwordConfirmControl(): FormControl {
    return this.loginForm.get('passwordConfirm') as FormControl;
  }

  get usernameControl(): FormControl {
    return this.loginForm.get('username') as FormControl;
  }

  get passwordControlInvalid(): boolean {
    return this.passwordControl.hasError('required')
      && this.passwordControl.touched;
  }

  get usernameControlInvalid(): boolean {
    return this.usernameControl.hasError('required') && this.usernameControl.touched;
  }

  onSubmit(): void {
    const username = this.usernameControl.value;
    const plainPassword = this.passwordControl.value;
    this.authenticationservice.login(username, plainPassword).subscribe(() => {
      this.notify.success('Successfully logged in.', 'Success', true);
      const returnUrl = this.returnUrl !== null ? this.returnUrl : '/';
      this.router.navigate([returnUrl]).then(() => location.reload(true));
    }, (err) => {
      const message = err.hasOwnProperty('message') ? err.message + ': ' : '';
      this.notify.warn(message, err.title);
        });
  }
}
