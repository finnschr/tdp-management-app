import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { VerifyService } from '../../services/verify.service';
import { map, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-verification',
  templateUrl: './verification.component.html',
  styleUrls: ['./verification.component.scss']
})
export class VerificationComponent implements OnInit {
  state = 'input';
  constructor(private route: ActivatedRoute, private verificationService: VerifyService) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      const token = params.get('verificationToken');
      this.verificationService.sendToken(token)
      .pipe(
        map(() => {
          return true;
        }),
        catchError((error: any) => {
          this.state = 'denied';
          return Observable.create(false);
        })
        )
        .subscribe((result) => {
          if (result) {
            this.state = 'verified';
          } else {
            this.state = 'denied';
          }
        });
    });
  }
}
