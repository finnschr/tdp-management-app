import { Component } from '@angular/core';
import { FormBuilder} from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';

@Component({
  selector: 'app-logout',
  template: `
  <form formGroupName="loginForm">
    <div class="tdp-form-container">
      <h2 class="is-purpel mb-0">Tour de Planet.</h2>
      <h2 class="mt-0">Enough for today?</h2>
      <div class="tdp-form-row mt-2">
        <button (click)="onSubmit()" class="tdp-button tdp-button-menu full">Logout</button>
      </div>
    </div>
  </form>
  `
})
export class LogoutComponent {
  loginForm: FormBuilder;
  constructor(private authenticationservice: AuthenticationService, private router: Router) {}

  onSubmit(): void {
    this.authenticationservice.logout().subscribe(() => {
      this.router.navigate(['']).then(() => location.reload(true));
    });
  }
}
