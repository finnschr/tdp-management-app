import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray} from '@angular/forms';
import { Subscription, Observable } from 'rxjs';
import { TourFormService } from '../../service/tour-form.service';
import { TourModel, TourType, TourState } from 'src/app/shared/models/tour.model';
import { Store } from '@ngrx/store';
import { ActivatedRoute } from '@angular/router';
import { DataService } from 'src/app/shared/services';
import { MeanOfTransportationModel } from 'src/app/shared/models/mean_of_transportaion.model';
import * as fromTourStore from '../../../../shared/store';
import * as fromAttendeeStore from 'src/app/shared/store';
import { TourService } from 'src/app/shared/services/tour.service';

@Component({
  selector: 'app-tour-edit.component',
  templateUrl: './tour-edit.component.component.html',
  styleUrls: ['./tour-edit.component.component.scss']
})
export class TourEditComponent implements OnInit {
  meanOfTransportation$: Observable<MeanOfTransportationModel[]>;
  tour: TourModel;
  tourForm: FormGroup;
  tourFormSub: Subscription;
  formInvalid = false;
  tourSegments: FormArray;
  start: FormGroup;
  end: FormGroup;
  organizer: FormGroup;
  types = [TourType.BIKE, TourType.WALK, TourType.SCOOTER, TourType.RIDE_HORSE];
  states = [TourState.DRAFT, TourState.INTERNAL, TourState.PUBLISHED, TourState.VERIFIED];

  constructor(
    private tourFormService: TourFormService,
    private store: Store<fromTourStore.AppState>,
    private route: ActivatedRoute,
    private tourService: TourService
    ) { }

  ngOnInit(): void {
    if (this.tour) {
      this.route.queryParamMap.subscribe(params => {
        if (params.hasOwnProperty('id')) {
          // tslint:disable-next-line:no-string-literal
          const id = params['id'];
          this.tourService.get(id).subscribe(tour => {
            this.tour = tour;
            this.tourFormService.init(this.tour);
          });
        }
      });

      this.meanOfTransportation$ = this.store.select(fromAttendeeStore.getMeansOfTransportation);

      this.tourFormSub = this.tourFormService.tourForm$.subscribe(form => {
        this.tourForm = form;
        this.tourSegments = this.tourForm.get('tourSegments') as FormArray;
        this.organizer = this.tourForm.get('organizer') as FormGroup;
        this.start = this.tourForm.get('start') as FormGroup;
        this.end = this.tourForm.get('end') as FormGroup;
      });
    }
  }

  addSegment() {
    this.tourFormService.addSegment();
  }

  deleteSegment(index: number): void {
    this.tourFormService.deleteSegment(index);
  }

  updateTour(): void {
    this.store.dispatch(new fromTourStore.UpdateTour(this.tourFormService.getModelData()));
  }

  createTour(): void {
    this.store.dispatch(new fromTourStore.CreateTour(this.tourFormService.getModelData()));
  }

  deleteTour(): void {
    this.store.dispatch(new fromTourStore.RemoveTour(this.tour));
  }
}
