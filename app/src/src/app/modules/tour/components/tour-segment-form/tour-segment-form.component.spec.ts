import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TourSegmentFormComponent } from './tour-segment-form.component';

describe('TourSegmentFormComponent', () => {
  let component: TourSegmentFormComponent;
  let fixture: ComponentFixture<TourSegmentFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TourSegmentFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TourSegmentFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
