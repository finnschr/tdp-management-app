import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TourPointFormComponent } from './tour-point-form.component';

describe('TourPointFormComponent', () => {
  let component: TourPointFormComponent;
  let fixture: ComponentFixture<TourPointFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TourPointFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TourPointFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
