import { TestBed } from '@angular/core/testing';

import { MapBoxWebGLService } from './map-box-web-gl.service';

describe('MapBoxWebGLService', () => {
  let service: MapBoxWebGLService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MapBoxWebGLService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
