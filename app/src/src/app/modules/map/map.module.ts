import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MapComponent } from './map.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { MatListModule, MatCardModule } from '@angular/material';

@NgModule({
  declarations: [MapComponent],
  imports: [
    CommonModule,
    SharedModule,
    HttpClientModule,
    HttpModule,
    MatListModule,
    MatCardModule,
  ],
  providers: [],
  exports: [MapComponent]
})
export class MapModule { }
