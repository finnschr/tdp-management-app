import { Component, Input, OnInit } from '@angular/core';
import { TourEvent } from 'src/app/shared/models/tour_event.model';
import { TourSegmentModel } from 'src/app/shared/models/tour_segment.model';
import { TourModel } from '../../shared/models/tour.model';
import { Store } from '@ngrx/store';
import * as fromMapStore from 'src/app/shared/store';
import * as fromAttendeeStore from 'src/app/shared/store';
import { MapBoxWebGLService } from './services/map-box-web-gl.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {
  @Input() zoom: number;
  @Input() latitude: number;
  @Input() longitude: number;
  eventsInPopup: TourEvent[] = [];
  segmentsInPopup: TourSegmentModel[] = [];
  toursInPopup: TourModel[] = [];
  constructor(private store: Store<fromMapStore.AppState>,  private mapService: MapBoxWebGLService ) { }

  ngOnInit(): void {
    this.mapService.initMap(
      {
        zoom: this.zoom,
        latitude: this.latitude,
        longitude: this.longitude,
        popup: 'popup',
        map: 'map',
        controls: 'controls',
        clusterDistance: 40,
      }
    );

    this.store.dispatch(new fromMapStore.LoadMapAction());
    this.store.dispatch(new fromAttendeeStore.LoadMeansOfTransportationAction());
    this.store.select(fromMapStore.getAllToursOnMap).subscribe(tours => {
      // tours.forEach(tour => {
      //   this.olMap.addTourToMap(tour);
      // });
    });
    this.store.select(fromMapStore.getAllEventsOnMap).subscribe(events => {
      // this.olMap.addEventsToMap(events);
    });

    // this.olMap.onSingleClick((event: {events: TourEvent[], segments: TourSegmentModel[], tours: TourModel[]}) => {
    //   event.events.forEach(e => this.store.dispatch(new EventSelectedAction(e)));
    //   event.segments.forEach(s => this.store.dispatch(new SegmentSelectedAction(s)));
    //   event.tours.forEach(t => this.store.dispatch(new TourSelectedAction(t)));
    // });
  }

  getEventTitle(event: TourEvent, short: boolean): string {
    let title = (event.author !== undefined ? event.author + ': ' : '') + event.title + ', ';
    title += (event.point.name ? event.point.name + ', ' : '');
    if (short) {
      return title;
    }
    title += new Date(event.date_from * 1000)
    .toLocaleString(undefined, {year: 'numeric', month: 'long', day: '2-digit', hour: '2-digit', minute: '2-digit'});
    if (event.date_to) {
      title += ' - ' + new Date(event.date_to * 1000)
      .toLocaleString(undefined, {year: 'numeric', month: 'long', day: '2-digit', hour: '2-digit', minute: '2-digit'});
    }
    return title;
  }

  getSegmentTitle(segment: TourSegmentModel): string {
    let title = '';
    if (segment.start.name || segment.end.name) {
      title += '(' + (segment.start.name ? segment.start.name + ' - ' : '') + segment.end.name + ')';
    }
    if (segment.tour) {
      title += ' - Tour: ' + segment.tour.title;
      if (segment.tour.start.name || segment.tour.end.name) {
        title += ' (' + (segment.tour.start.name ? segment.tour.start.name + ' - ' : '') + segment.tour.end.name + ')';
      }
    }
    return title;
  }
}
