import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { Unit, TourAttendeeDonation } from 'src/app/shared/models/tour_attendee_donation.model';
import { Store } from '@ngrx/store';
import * as fromDonationsStore from 'src/app/shared/store';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-counter',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.scss']
})
export class CounterComponent implements OnInit, OnDestroy {
  unit = 'km';
  sum = 0;
  private dSubscriber: Subscription;
  constructor(private store: Store<fromDonationsStore.AppState>, private ref: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.dSubscriber = this.store.select(fromDonationsStore.getAllAttendeeDonations).subscribe(donations => {
      this.unit = this.evaluateUnit(donations);
      this.sum = this.evaluateSum(donations);
      this.ref.detectChanges();
    });
  }
  ngOnDestroy(): void {
    this.dSubscriber.unsubscribe();
  }

  private evaluateUnit(donations: TourAttendeeDonation[]): string {
    let unit: Unit;
    donations.forEach(d => {
      unit = d.unit;
    });

    return unit;
  }
  private evaluateSum(donations: TourAttendeeDonation[]): number {
    if (!donations) {
      return 0;
    }

    return donations.reduce((sum, donation) => {
      sum += donation.amount;
      return sum;
    }, 0);
  }
}
