import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TotalDonationsComponent } from './total-donations.component';

describe('TotalDonationsComponent', () => {
  let component: TotalDonationsComponent;
  let fixture: ComponentFixture<TotalDonationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TotalDonationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalDonationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
