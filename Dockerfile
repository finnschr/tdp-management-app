FROM node:13 as node-builder

WORKDIR /app/src/
# install and cache app dependencies
COPY / /

# add `/app/src/node_modules/.bin` to $PATH
ENV PATH /app/src/node_modules/.bin:$PATH

# install and cache app dependencies
RUN npm install &&  npm install -g @angular/cli@1.7.1 \
    # build app
    && ng build --prod



FROM node-builder as node-dev-base
# install chrome for protractor tests
RUN \
  wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - && \
  echo "deb http://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google.list && \
  apt-get update && \
  apt-get install -y google-chrome-stable && \
  rm -rf /var/lib/apt/lists/*


# add `/app/src/node_modules/.bin` to $PATH
ENV PATH /app/src/node_modules/.bin:$PATH


FROM registry.gitlab.com/froscon/php-track-web/alpine-nginx:1.1.0 as tdp-app-production

WORKDIR /app/src/public
COPY --from=node-builder /app/src/dist/tdp-editor /app/src/public

RUN apk update && apk add --no-cache acl

ENV VERSION_TAG 0.0.1
LABEL image.name=tdp-app-production \
      image.version=0.0.1 \
      image.tag=registry.gitlab.com/developersforfuture/tdp-tourmanagement-app/tdp-app-development \
      image.scm.commit=$commit \
      image.scm.url=git@gitlab.com:DevelopersForFuture/tdp-tourmanagement-app.git \
      image.author="Maximilian Berghoff <maximilian.berghoff@gmx.de>"


FROM node-dev-base as tdp-app-development

WORKDIR /app/src

COPY / /
COPY --from=node-builder /app/src/node_modules/ /app/src/node_modules/
COPY --from=node-builder /app/src/package-lock.json /app/src/package-lock.json

EXPOSE 4200

# start app
CMD ["npm" "run start:dev"]

ENV VERSION_TAG 0.0.1
LABEL image.name=tdp-app-development \
      image.version=0.0.1 \
      image.tag=registry.gitlab.com/developersforfuture/tdp-tourmanagement-app/tdp-app-development \
      image.scm.commit=$commit \
      image.scm.url=git@gitlab.com:DevelopersForFuture/tdp-tourmanagement-app.git \
      image.author="Maximilian Berghoff <maximilian.berghoff@gmx.de>"
